/* A word guessing game for the web.
 * Copyright (C) 2021  Yuval Langer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


function clear_canvas() {
	window.ctx.clearRect(0, 0, canvas.width, canvas.height);
};

function draw_message(structured_message) {
	console.log(structured_message);

	if (structured_message.type == 'begin_path') {
		window.last_path_point = structured_message.value;

		console.log('mooooo', structured_message);
	} else if ((structured_message.type == 'continue_path') ||
		(structured_message.type == 'end_path')) {
		console.log('mooooo', structured_message);

		window.ctx.beginPath();
		window.ctx.moveTo(window.last_path_point[0], window.last_path_point[1]);
		window.ctx.lineTo(structured_message.value[0], structured_message.value[1]);
		window.ctx.lineWidth = 4;
		window.ctx.strokeStyle = '#000';
		window.ctx.stroke();

		window.last_path_point = structured_message.value;
	};
}

function message_handler(e) {
	let structured_message = JSON.parse(e.data);
	console.log(structured_message);

	switch (structured_message.type) {
		case 'begin_path':
		case 'continue_path':
		case 'end_path':
			draw_message(structured_message);
			break;
		case 'illustrator_status':
			window.we_are_illustrator = structured_message.value;
			break;
		case 'clear_canvas':
			clear_canvas();
			break;
		case 'guess':
			let guess = structured_message.value;

			window.guesses.push(guess);

			let guesses = document.getElementById('guesses');

			guesses.innerText += `\n${guess}`;

			break;
	};
};

let current_path = [];

function touchstart_handler(e) {};
function touchend_handler(e) {};
function touchcancel_handler(e) {};
function touchmove_handler(e) {};

function mousedown_handler(e) {
	if (window.we_are_illustrator) {
		let mouse_on_canvas_coordinates = get_mouse_on_canvas_coordinates(e);

		let structured_message = {
			'type': 'begin_path',
			'value': mouse_on_canvas_coordinates,
		};
		let serialized_message = JSON.stringify(structured_message);
		console.log(serialized_message);
		window.our_websocket.send(serialized_message);
		window.last_path_point = structured_message.value;
	};
};

function mousemove_handler(e) {
	// The lessermost bit of e.buttons is 1 if primary mouse button is currently pressed.
	if (window.we_are_illustrator && ((1 & e.buttons) === 1)) {
		let mouse_on_canvas_coordinates = get_mouse_on_canvas_coordinates(e);

		let structured_message = {
			'type': "continue_path",
			'value': mouse_on_canvas_coordinates,
		};
		window.our_websocket.send(JSON.stringify(structured_message));
	};
};

function mouseup_handler(e) {
	if (window.we_are_illustrator) {
		let mouse_on_canvas_coordinates = get_mouse_on_canvas_coordinates(e);

		let structured_message = {
			'type': "end_path",
			'value': mouse_on_canvas_coordinates
		};
		window.our_websocket.send(JSON.stringify(structured_message));
	};
};

function clear_button_press_handler(e) {
	if (window.we_are_illustrator) {
		let structured_message = {'type': 'clear_canvas'};
		let serialized_message = JSON.stringify(structured_message);
		window.our_websocket.send(serialized_message);

		clear_canvas();
	};
}


function guess_submit_handler(e) {
	e.preventDefault();

	let guess_input = document.getElementById('guess_input');

	let guess = guess_input.value;
	
	let structured_message = {
		'type': 'guess',
		'value': guess,
	};

	let serialized_message = JSON.stringify(structured_message);

	console.log(serialized_message);

	window.our_websocket.send(serialized_message);

	guess_input.value = '';
};


function get_mouse_on_canvas_coordinates(event) {
	// TODO: This looks wonky.
	let rect = window.canvas.getBoundingClientRect();
	return [event.offsetX - rect.left, event.offsetY - rect.top];
};


function load_handler() {
	window.our_websocket = new WebSocket("ws://localhost:20001");
	window.our_websocket.addEventListener('message', message_handler);
	window.guesses = [];

	window.canvas = document.querySelector('#canvas');
	window.ctx = window.canvas.getContext('2d');

	canvas.addEventListener('touchstart', touchstart_handler);
	canvas.addEventListener('touchend', touchend_handler);
	canvas.addEventListener('touchcancel', touchcancel_handler);
	canvas.addEventListener('touchmove', touchmove_handler);

	canvas.addEventListener('mousedown', mousedown_handler);
	canvas.addEventListener('mousemove', mousemove_handler);
	canvas.addEventListener('mouseup', mouseup_handler);

	let clear_button = document.getElementById('clear_button');

	clear_button.addEventListener('click', clear_button_press_handler);

	let guess_form = document.getElementById('guess_form');
	let guess_input = document.getElementById('guess_input');
	guess_form.addEventListener('submit', guess_submit_handler);
};

window.addEventListener('load', load_handler);

# A word guessing game for the web.
# Copyright (C) 2021  Yuval Langer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from typing import Tuple, Union, Literal

import pydantic


class BeginPath(pydantic.BaseModel):
    type: Literal["begin_path"] = "begin_path"
    value: Tuple[float, float]


class ContinuePath(pydantic.BaseModel):
    type: Literal["continue_path"] = "continue_path"
    value: Tuple[float, float]


class EndPath(pydantic.BaseModel):
    type: Literal["end_path"] = "end_path"
    value: Tuple[float, float]


PathEvent = Union[BeginPath, ContinuePath, EndPath]


class IllustratorStatus(pydantic.BaseModel):
    type: Literal["illustrator_status"] = "illustrator_status"
    value: Literal[True] = True


class GuesserStatus(pydantic.BaseModel):
    type: Literal["illustrator_status"] = "illustrator_status"
    value: Literal[False] = False


class ClearCanvas(pydantic.BaseModel):
    type: Literal["clear_canvas"] = "clear_canvas"


class Guess(pydantic.BaseModel):
    type: Literal["guess"] = "guess"
    value: str


if __name__ == "__main__":
    begin = {"type": "begin_path", "value": (1.2, 2.1)}
    proceed = {"type": "proceed_path", "value": (1.2, 2.1)}
    end = {"type": "end_path", "value": (1.2, 2.1)}
    print(begin)
    print(proceed)
    print(end)

    print(BeginPath(**begin))
    print(ContinuePath(**proceed))
    print(EndPath(**end))

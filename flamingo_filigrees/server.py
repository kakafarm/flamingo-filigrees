# A word guessing game for the web.
# Copyright (C) 2021  Yuval Langer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from typing import Dict, List, Optional, Tuple, Generator
import json
import typing

import pydantic
import starlette
import fastapi
import fastapi.staticfiles
import fastapi.templating

import our_types
from our_types import BeginPath, ContinuePath, EndPath, PathEvent


DRAWING: List[PathEvent] = []
GUESSES: List[our_types.Guess] = []


app = fastapi.FastAPI()


app.mount(
    "/static",
    fastapi.staticfiles.StaticFiles(directory="static"),
    "static",
)

templates = fastapi.templating.Jinja2Templates(directory="templates")


@app.get("/", response_class=fastapi.responses.HTMLResponse)
async def index(request: fastapi.Request) -> starlette.templating._TemplateResponse:
    template_response = templates.TemplateResponse("index.html", {"request": request})
    return template_response


class ConnectionManager:
    def __init__(self) -> None:
        self.active_connections: List[fastapi.WebSocket] = []

    async def connect(self, websocket: fastapi.WebSocket) -> None:
        await websocket.accept()
        self.active_connections.append(websocket)

    def disconnect(self, websocket: fastapi.WebSocket) -> None:
        self.active_connections.remove(websocket)

    def get_all_but(
        self, websocket: fastapi.WebSocket
    ) -> Generator[fastapi.WebSocket, None, None]:
        for connection in self.active_connections:
            if connection.client != websocket.client:
                yield connection

    def is_illustrator(self, websocket: fastapi.WebSocket) -> bool:
        return self.active_connections[0] == websocket

    async def refresh_illustrator(self) -> None:
        if len(self.active_connections) > 0:
            print(our_types.IllustratorStatus())

            print(our_types.IllustratorStatus().dict())

            await self.active_connections[0].send_json(
                our_types.IllustratorStatus().dict()
            )

        for connection in self.active_connections[1:]:
            await connection.send_json(our_types.GuesserStatus().dict())


manager = ConnectionManager()


async def replay_drawing_events(websocket: fastapi.WebSocket) -> None:
    for path_event in DRAWING:
        await websocket.send_json(path_event.dict())


async def try_path_event(
    our_websocket: fastapi.WebSocket,
    structured_message: Dict[str, typing.Any],
) -> None:
    if manager.is_illustrator(our_websocket):
        path_event: Optional[PathEvent] = None

        try:
            path_event = BeginPath(**structured_message)
        except pydantic.ValidationError:
            pass

        try:
            path_event = ContinuePath(**structured_message)
        except pydantic.ValidationError:
            pass

        try:
            path_event = EndPath(**structured_message)
        except pydantic.ValidationError:
            pass

        if path_event is not None:
            DRAWING.append(path_event)

            # send the event to everyone, including ourselves.
            for other_websocket in manager.active_connections:
                await other_websocket.send_json(path_event.dict())


async def try_clear_canvas(
    websocket: fastapi.WebSocket,
    structured_message: Dict[str, typing.Any],
) -> None:
    if manager.is_illustrator(websocket):
        clear: Optional[our_types.ClearCanvas] = None

        try:
            clear = our_types.ClearCanvas(**structured_message)
        except pydantic.ValidationError:
            return

        DRAWING.clear()

        for connection in manager.get_all_but(websocket):
            await connection.send_json(clear.dict())


async def try_guess(
    websocket: fastapi.WebSocket,
    structured_message: Dict[str, typing.Any],
) -> None:
    if not manager.is_illustrator(websocket):
        guess: Optional[our_types.Guess] = None

        try:
            guess = our_types.Guess(**structured_message)
        except pydantic.ValidationError:
            return

        GUESSES.append(guess)

        for connection in manager.active_connections:
            await connection.send_json(guess.dict())


@app.websocket_route("/")
async def websocket_stuff(websocket: fastapi.WebSocket) -> None:
    await manager.connect(websocket)

    if manager.is_illustrator(websocket):
        await websocket.send_json(our_types.IllustratorStatus().dict())
    else:
        await websocket.send_json(our_types.GuesserStatus().dict())

    await replay_drawing_events(websocket)

    try:
        while True:
            # Make sure we are getting a JSON message.
            try:
                structured_message = await websocket.receive_json()
            except json.JSONDecodeError:
                continue

            await try_path_event(websocket, structured_message)
            await try_clear_canvas(websocket, structured_message)
            await try_guess(websocket, structured_message)

    except fastapi.WebSocketDisconnect:
        manager.disconnect(websocket)
        await manager.refresh_illustrator()
